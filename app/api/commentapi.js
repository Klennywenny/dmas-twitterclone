/**
 * Created by Daniel on 19.11.2016.
 */
'use strict';

const Comment = require('../models/comment');
const Boom = require('boom');

exports.find = {

  auth: false,

  handler: function (request, reply) {
    Comment.find({}).then(comments => {
      reply(comments);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.findOne = {

  auth: false,

  handler: function (request, reply) {
    Comment.findOne({ _id: request.params.id }).then(comment => {
      if (comment != null) {
        reply(comment);
      }

      reply(Boom.notFound('id not found'));
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};

exports.findByUser = {

  auth: false,

  handler: function (request, reply) {
    Comment.find({ author: request.params.id }).then(comments => {
      reply(comments);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.findByTweet = {

  auth: false,

  handler: function (request, reply) {
    Comment.find({ tweet: request.params.id }).then(comments => {
      reply(comments);
    }).catch(err => {
      reply(Boom.badImplementation('error accessing db'));
    });
  },

};

exports.create = {

  auth: false,

  handler: function (request, reply) {
    const comment = new Comment(request.payload);
    comment.save().then(newComment => {
      reply(newComment).code(201);
    }).catch(err => {
      reply(Boom.badImplementation('error creating Comment'));
    });
  },

};

exports.deleteAll = {

  auth: false,

  handler: function (request, reply) {
    Comment.remove({}).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Comments'));
    });
  },

};

exports.deleteOne = {

  auth: false,

  handler: function (request, reply) {
    Comment.remove({ _id: request.params.id }).then(comment => {
      reply(Comment).code(204);
    }).catch(err => {
      reply(Boom.notFound('id not found'));
    });
  },

};

exports.deleteAllByUser = {

  auth: false,

  handler: function (request, reply) {
    Comment.remove({ author: request.params.id }).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Comments of this User'));
    });
  },

};

exports.deleteAllByTweet = {

  auth: false,

  handler: function (request, reply) {
    Comment.remove({ tweet: request.params.id }).then(err => {
      reply().code(204);
    }).catch(err => {
      reply(Boom.badImplementation('error removing Comments of this Tweet'));
    });
  },

};
