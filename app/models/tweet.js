/**
 * Created by Daniel on 19.11.2016.
 */
'use strict';

const mongoose = require('mongoose');

const tweetSchema = mongoose.Schema({
  author: { type: mongoose.Schema.Types.ObjectId, ref: 'User' },
  content: String,
  date: { type: Date, default: Date.now() },
});

const Tweet = mongoose.model('Tweet', tweetSchema);
module.exports = Tweet;
