/**
 * Created by Daniel on 19.11.2016.
 */
'use strict';

const mongoose = require('mongoose');

const userSchema = mongoose.Schema({
  firstName: String,
  lastName: String,
  email: String,
  password: String,
  accountType: String,
});

const User = mongoose.model('User', userSchema);
module.exports = User;
