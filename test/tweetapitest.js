/**
 * Created by Daniel on 19.11.2016.
 */
'use strict';

const assert = require('chai').assert;
const TwitterService = require('./twitter-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Tweet API tests', function () {

  let tweets = fixtures.tweets;
  let newTweet = fixtures.newTweet;

  const twitterService = new TwitterService(fixtures.twitterService);

  beforeEach(function () {
    twitterService.deleteAllTweets();
  });

  afterEach(function () {
    twitterService.deleteAllTweets();
  });

  test('create a tweet', function () {
    const returnedTweet = twitterService.createTweet(newTweet);
    assert(_.some([returnedTweet], newTweet), 'returnedTweet must be a superset of newTweet');
    assert.isDefined(returnedTweet._id);
  });

  test('get tweet', function () {
    const c1 = twitterService.createTweet(newTweet);
    const c2 = twitterService.getTweet(c1._id);
    assert.deepEqual(c1, c2);
  });

  test('get invalid tweet', function () {
    const c1 = twitterService.getTweet('1234');
    assert.isNull(c1);
    const c2 = twitterService.getTweet('012345678901234567890123');
    assert.isNull(c2);
  });

  test('delete a tweet', function () {
    const c = twitterService.createTweet(newTweet);
    assert(twitterService.getTweet(c._id) != null);
    twitterService.deleteOneTweet(c._id);
    assert(twitterService.getTweet(c._id) == null);
  });

  test('get all tweets', function () {
    for (let c of tweets) {
      twitterService.createTweet(c);
    }

    const allTweets = twitterService.getTweets();
    assert.equal(allTweets.length, tweets.length);
  });

  test('get tweets detail', function () {
    for (let c of tweets) {
      twitterService.createTweet(c);
    }

    const allTweets = twitterService.getTweets();
    for (var i = 0; i < tweets.length; i++) {
      assert(_.some([allTweets[i]], tweets[i]), 'returnedTweet must be a superset of newTweet');
    }
  });

  test('get all tweets empty', function () {
    const allTweets = twitterService.getTweets();
    assert.equal(allTweets.length, 0);
  });
});
