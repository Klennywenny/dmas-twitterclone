/**
 * Created by Daniel on 19.11.2016.
 */
'use strict';

const SyncHttpService = require('./sync-http-client');
const baseUrl = 'http://localhost:4000';

class TwitterService {

  constructor(baseUrl) {
    this.httpService = new SyncHttpService(baseUrl);
  }

  getUsers() {
    return this.httpService.get('/api/users');
  }

  getUser(id) {
    return this.httpService.get('/api/users/' + id);
  }

  createUser(newUser) {
    return this.httpService.post('/api/users', newUser);
  }

  deleteAllUsers() {
    return this.httpService.delete('/api/users');
  }

  deleteOneUser(id) {
    return this.httpService.delete('/api/users/' + id);
  }

  getTweets() {
    return this.httpService.get('/api/tweets');
  }

  getTweetsByUser(id) {
    return this.httpService.get('/api/users/' + id + '/tweets');
  }

  getTweet(id) {
    return this.httpService.get('/api/tweets/' + id);
  }

  createTweet(newTweet) {
    return this.httpService.post('/api/tweets', newTweet);
  }

  deleteAllTweets() {
    return this.httpService.delete('/api/tweets');
  }

  deleteAllTweetsByUser(id) {
    return this.httpService.delete('/api/users/' + id + '/tweets');
  }

  deleteOneTweet(id) {
    return this.httpService.delete('/api/tweets/' + id);
  }

  createComment(tweetid, comment) {
    return this.httpService.post('/api/tweets/' + tweetid + '/comments', comment);
  }

  getCommentsByUser(id) {
    return this.httpService.get('/api/users/' + id + '/comments');
  }

  getCommentsByTweet(id) {
    return this.httpService.get('/api/tweets/' + id + '/comments');
  }

  getComments() {
    return this.httpService.get('/api/comments');
  }

  getComment(id) {
    return this.httpService.get('/api/comments/' + id);
  }

  deleteAllComments() {
    return this.httpService.delete('/api/comments');
  }

  deleteOneComment(id) {
    return this.httpService.delete('/api/comments/' + id);
  }

  deleteAllCommentsByUser(id) {
    return this.httpService.delete('/api/users/' + id + '/comments');
  }

  deleteAllCommentsByTweet(id) {
    return this.httpService.delete('/api/tweets/' + id + '/comments');
  }
}

module.exports = TwitterService;
