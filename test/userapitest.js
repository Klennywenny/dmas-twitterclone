/**
 * Created by Daniel on 19.11.2016.
 */
'use strict';

const assert = require('chai').assert;
const TwitterService = require('./twitter-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Users API tests', function () {

  let users = fixtures.users;
  let newUser = fixtures.newUser;

  const twitterService = new TwitterService(fixtures.twitterService);

  beforeEach(function () {
    twitterService.deleteAllUsers();
  });

  afterEach(function () {
    twitterService.deleteAllUsers();
  });

  test('create a user', function () {
    const returnedUser = twitterService.createUser(newUser);
    assert(_.some([returnedUser], newUser), 'returnedCandidate must be a superset of newCandidate');
    assert.isDefined(returnedUser._id);
  });

  test('get user', function () {
    const c1 = twitterService.createUser(newUser);
    const c2 = twitterService.getUser(c1._id);
    assert.deepEqual(c1, c2);
  });

  test('get invalid user', function () {
    const c1 = twitterService.getUser('1234');
    assert.isNull(c1);
    const c2 = twitterService.getUser('012345678901234567890123');
    assert.isNull(c2);
  });

  test('delete a user', function () {
    const c = twitterService.createUser(newUser);
    assert(twitterService.getUser(c._id) != null);
    twitterService.deleteOneUser(c._id);
    assert(twitterService.getUser(c._id) == null);
  });

  test('get all users', function () {
    for (let c of users) {
      twitterService.createUser(c);
    }

    const allUsers = twitterService.getUsers();
    assert.equal(allUsers.length, users.length);
  });

  test('get users detail', function () {
    for (let c of users) {
      twitterService.createUser(c);
    }

    const allUsers = twitterService.getUsers();
    for (var i = 0; i < users.length; i++) {
      assert(_.some([allUsers[i]], users[i]), 'returnedCandidate must be a superset of newCandidate');
    }
  });

  test('get all users empty', function () {
    const allUsers = twitterService.getUsers();
    assert.equal(allUsers.length, 0);
  });
});
