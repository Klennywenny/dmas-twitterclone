/**
 * Created by Daniel on 19.11.2016.
 */
'use strict';

const assert = require('chai').assert;
const TwitterService = require('./twitter-service');
const fixtures = require('./fixtures.json');
const _ = require('lodash');

suite('Tweet API tests', function () {

  let comments = fixtures.comments;
  let newComment = fixtures.newComment;
  let newTweet = fixtures.newTweet;

  const twitterService = new TwitterService(fixtures.twitterService);

  beforeEach(function () {
    twitterService.deleteAllComments();
    twitterService.deleteAllTweets();
  });

  afterEach(function () {
    twitterService.deleteAllComments();
    twitterService.deleteAllTweets();
  });

  test('create a comment', function () {
    const returnedComment = twitterService.createComment(newTweet, newComment);
    assert(_.some([returnedComment], newComment), 'returnedComment must be a superset of newComment');
    assert.isDefined(returnedComment._id);
  });

  test('get comment', function () {
    const c1 = twitterService.createComment(newComment);
    const c2 = twitterService.getComment(c1._id);
    assert.deepEqual(c1, c2);
  });

  test('get invalid comment', function () {
    const c1 = twitterService.getComment('1234');
    assert.isNull(c1);
    const c2 = twitterService.getComment('012345678901234567890123');
    assert.isNull(c2);
  });

  test('delete a comment', function () {
    const c = twitterService.createComment(newComment);
    assert(twitterService.getComment(c._id) != null);
    twitterService.deleteOneComment(c._id);
    assert(twitterService.getComment(c._id) == null);
  });

  test('get all comments', function () {
    for (let c of comments) {
      twitterService.createComment(c);
    }

    const allComments = twitterService.getComments();
    assert.equal(allComments.length, comments.length);
  });

  test('get comments detail', function () {
    for (let c of comments) {
      twitterService.createComment(newTweet, c);
    }

    const allComments = twitterService.getComments();
    for (var i = 0; i < comments.length; i++) {
      assert(_.some([allComments[i]], comments[i]), 'returnedComment must be a superset of newComment');
    }
  });

  test('get all comments empty', function () {
    const allComments = twitterService.getComments();
    assert.equal(allComments.length, 0);
  });
});
